package com.example.crib;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void firstButton(View view) {
        Intent intent = new Intent(this, FirtsPage.class);
        startActivity(intent);
    }

    public void secondButton(View view) {
        Intent intent = new Intent(this, FirtsPage.class);
        startActivity(intent);
    }

    public void thirdButton(View view) {
        Intent intent = new Intent(this, FirtsPage.class);
        startActivity(intent);
    }
}
